use datatransformation2;

/*

select count(playback.song_id), songs.title from playback
	join songs on playback.song_id = songs.song_id
	group by playback.song_id
    order by count(playback.song_id) desc;
    
select songs.title, count(playback.song_id) from songs
join playback
	group by playback.song_id
    order by count(playback.song_id) desc;
    
*/

select p.counted, p.song_id, songs.title from
(select song_id, count(playback.song_id) as 'counted' from playback
	group by playback.song_id) as p
    join songs ON songs.song_id = p.song_id
    order by p.counted desc