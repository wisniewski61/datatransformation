use datatransformation2;

SET @queen_id := 
	(SELECT artist_id FROM artists
	where artists.artist_name like 'Queen'
    LIMIT 1);

drop table if exists selected_songs;

create temporary table selected_songs as
	(select songs.song_id, songs.title, p.counted from
		(select song_id, count(playback.song_id) as 'counted' from playback
			where playback.artist_id = @queen_id
			group by playback.song_id
			order by count(playback.song_id) desc) as p
		join songs ON songs.song_id = p.song_id
		limit 3);

select playback.user_id from selected_songs
	join playback on playback.song_id = selected_songs.song_id
    group by playback.user_id