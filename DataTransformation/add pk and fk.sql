/* Add pk and fk to necessary tables */
use datatransformation2;

SET foreign_key_checks=0;
SET unique_checks=0;

ALTER TABLE playback
  ADD PRIMARY KEY (id);

ALTER TABLE artists
  ADD PRIMARY KEY (artist_id); 

ALTER TABLE dates
  ADD PRIMARY KEY (id); 
  
ALTER TABLE songs
  ADD PRIMARY KEY (song_id); 
  
ALTER TABLE users
  ADD PRIMARY KEY (id); 


ALTER TABLE playback
 ADD foreign key (user_id) references users (id),
 ADD foreign key (song_id) references songs (song_id),
 ADD foreign key (artist_id) references artists (artist_id),
 ADD foreign key (date_id) references dates (id);

ALTER TABLE songs
 ADD foreign key (artist_id) references artists (artist_id);
 
SET foreign_key_checks=1;
SET unique_checks=1;