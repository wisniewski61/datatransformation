﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransformation.DbConnectionHelpers
{
    public class TripletsLoader: UniqueTracksLoader
    {
        static readonly DateTime _unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        protected int lastDateId, lastUserId, lastPlaybackId;
        protected Dictionary<string, int> userKeys;
        bool isDate, isUser, isPlayback;
        //StringBuilder dateSb, usersSb, playbackSb;
        string dateQueryStr, usersQueryStr, playbackQueryStr;

        public TripletsLoader(string connectionString, Dictionary<string, int> songKeys, Dictionary<string, int> artistsKeys, Dictionary<int, int> songsArtists)
            : base(connectionString)
        {
            this.songKeys = songKeys;
            this.artistsKeys = artistsKeys;
            this.songsArtists = songsArtists;
            lastDateId = 0;
            lastUserId = 0;
            lastPlaybackId = 0;
            userKeys = new Dictionary<string, int>();
        }

        /*public override string InsertData()
        {
            if (values.Count == 0)
                return "No data to insert";

            if (!maxPacketSizeSet)
            {
                NonQuery("SET GLOBAL max_allowed_packet = 1073741824");
                maxPacketSizeSet = true;
            }//if

            BuildQueries();
            CleanMemory();

            // dates
            string query = GetDatesQuery();
            string queryResults = NonQuery(query);
            CleanMemory();

            // users
            query = GetUsersQuery();
            queryResults += " " + NonQuery(query);
            CleanMemory();

            // playback
            query = GetPlaybackQuery();
            queryResults += " " + NonQuery(query);
            CleanMemory();

            return queryResults;
        }*/

        private void CleanMemory()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        protected override string NonQuery(string commandText)
        {
            if(commandText.Length > 0)
                return base.NonQuery(commandText);
            return "-1";
        }

        protected override string BuildQuery()
        {
            StringBuilder dateQuery = new StringBuilder("INSERT INTO ").Append("dates").Append(" VALUES ");
            StringBuilder usersQuery = new StringBuilder("INSERT INTO ").Append("users").Append(" VALUES ");
            StringBuilder playbackQuery = new StringBuilder("INSERT INTO ").Append("playback").Append(" VALUES ");
            isDate = false;
            isUser = false;
            isPlayback = false;

            for (int i = 0; i < values.Count; i++)
            {
                string[] val = values[i];
                string origUserId = val[0];
                string origSongId = val[1];
                string origDate = val[2];

                int dateId = UpdateDateQuery(dateQuery, origDate);
                int userId = UpdateUsersQuery(usersQuery, origUserId);
                int songId = songKeys[origSongId];
                int artistId = songsArtists[songId];

                UpdatePlaybackQuery(playbackQuery, dateId, userId, songId, artistId);
            }//for

            values = new List<string[]>();
            return ConcatQueries(dateQuery, usersQuery, playbackQuery, isDate, isUser, isPlayback);
        }

        public string GetUsersQuery()
        {
            string query = usersQueryStr;
            usersQueryStr = null;
            return query;
            /*string query = "";
            if(isUser)
                query = usersSb.ToString();
            usersSb = null;
            isUser = false;
            return query;*/
        }

        public string GetDatesQuery()
        {
            string query = dateQueryStr;
            dateQueryStr = null;
            return query;
            /*
            string query = "";
            if (isDate)
                query = dateSb.ToString();
            dateSb = null;
            isDate = false;
            return query;
            */
        }

        public string GetPlaybackQuery()
        {
            string query = playbackQueryStr;
            playbackQueryStr = null;
            return query;
            /*
            string query = "";
            if (isPlayback)
                query = playbackSb.ToString();
            playbackSb = null;
            isPlayback = false;
            return query;
            */
        }

        private string ConcatQueries(StringBuilder dateQuery, StringBuilder usersQuery, StringBuilder playbackQuery, bool isDate, bool isUser, bool isPlayback)
        {
            StringBuilder sb = new StringBuilder();
            if (isDate)
                sb.Append(dateQuery);

            if (isUser)
            {
                if (isDate)
                    sb.Append(";");
                sb.Append(usersQuery);
            }//if

            if (isPlayback)
            {
                if (isDate || isUser)
                    sb.Append(";");
                sb.Append(playbackQuery);
            }//if

            return sb.ToString();
        }

        private void UpdatePlaybackQuery(StringBuilder playbackQuery, int dateId, int userId, int songId, int artistId)
        {
            if (isPlayback)
                playbackQuery.Append(",");
            isPlayback = true;

            playbackQuery.Append("(").Append(lastPlaybackId).Append(",").Append(userId).Append(",").Append(songId)
                .Append(",").Append(artistId).Append(",").Append(dateId).Append(")");
            lastPlaybackId++;
        }

        private int UpdateUsersQuery(StringBuilder usersQuery, string origUserId)
        {
            int userId;
            if (userKeys.TryGetValue(origUserId, out userId))
            {
                return userId;
            }//if
            else
            {
                if (isUser)
                    usersQuery.Append(",");
                isUser = true;
                userId = lastUserId;
                lastUserId++;

                usersQuery.Append("(").Append(userId).Append(",\'").Append(origUserId).Append("\')");

                return userId;
            }//else
        }

        private int UpdateDateQuery(StringBuilder dateQuery, string date)
        {
            int dateId = lastDateId;
            lastDateId++;

            if(isDate)
            {
                dateQuery.Append(",");
            }//if

            long unixTime = long.Parse(date);
            DateTime dt = DateFromTimestamp(unixTime);
            dateQuery.Append("(").Append(dateId).Append(",").Append(dt.Year).Append(",").Append(dt.Month).Append(",")
                .Append(dt.Day).Append(",").Append(dt.Hour).Append(",").Append(unixTime).Append(")");

            isDate = true;
            return dateId;
        }

        // source: https://stackoverflow.com/questions/18964370/converting-ticks-to-datetime
        public static DateTime DateFromTimestamp(long timestamp)
        {
            return _unixEpoch.AddSeconds(timestamp);
        }
    }
}
