﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransformation.DbConnectionHelpers
{
    public class UniqueTracksLoader
    {
        protected string connectionString;
        protected int songId, artistId;
        protected List<string[]> values;
        protected bool maxPacketSizeSet;
        protected Dictionary<string, int> songKeys;
        protected Dictionary<string, int> artistsKeys;
        protected Dictionary<int, int> songsArtists;

        public UniqueTracksLoader(string connectionString)
        {
            this.connectionString = connectionString;
            songId = 0;
            values = new List<string[]>();
            maxPacketSizeSet = false;
            songKeys = new Dictionary<string, int>();
            artistsKeys = new Dictionary<string, int>();
            songsArtists = new Dictionary<int, int>();
        }

        public Dictionary<int, int> GetSongsArtists()
        {
            return songsArtists;
        }

        public Dictionary<String, int> GetSongKeys()
        {
            return songKeys;
        }

        public Dictionary<String, int> GetArtistsKeys()
        {
            return artistsKeys;
        }

        public virtual string InsertData()
        {
            if (values.Count == 0)
                return "No data to insert";

            if (!maxPacketSizeSet)
            {
                NonQuery("SET GLOBAL max_allowed_packet = 1073741824");
                maxPacketSizeSet = true;
            }//if

            string query = BuildQuery();
            return NonQuery(query);
        }

        protected virtual string BuildQuery()
        {
            StringBuilder songsQuery = new StringBuilder("INSERT INTO ").Append("Songs").Append(" VALUES ");
            StringBuilder artistsQuery = new StringBuilder("INSERT INTO ").Append("Artists").Append(" VALUES ");
            bool isArtistEmpty = true;
            bool isSong = false;
            for (int i = 0; i < values.Count; i++)
            {
                string[] val = values[i];

                int curArtistId = -1;
                if (!artistsKeys.ContainsKey(val[2]))
                {
                    if (!isArtistEmpty)
                        artistsQuery.Append(",");

                    curArtistId = GetArtistId();
                    artistsQuery.Append("(").Append(curArtistId).Append(",\'").Append(ReplaceSpecials(val[2]))
                        .Append("\')");
                    artistsKeys.Add(val[2], curArtistId);

                    isArtistEmpty = false;
                }//if

                if (curArtistId == -1)
                    curArtistId = artistsKeys[val[2]];

                if (songKeys.ContainsKey(val[1]))
                    continue;

                if (isSong)
                    songsQuery.Append(",");
                isSong = true;

                int curSongId = GetSongId();
                songKeys.Add(val[1], curSongId);
                songsArtists.Add(curSongId, curArtistId);

                songsQuery.Append("(").Append(curSongId).Append(",\'").Append(val[0])
                    .Append("\',").Append(curArtistId).Append(",\'")
                    .Append(ReplaceSpecials(val[3])).Append("\')");
            }//for

            if(!isArtistEmpty)
                songsQuery.Append(";").Append(artistsQuery);

            values = new List<string[]>();
            return songsQuery.ToString();
        }

        protected int GetArtistId()
        {
            int current = artistId;
            artistId++;
            return artistId;
        }

        protected string ReplaceSpecials(string v)
        {
            string result = v.Replace("'", "_");
            return result;
        }

        protected virtual string NonQuery(string commandText)
        {
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                con.Open();
                using (MySqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = commandText;
                    cmd.CommandTimeout = int.MaxValue;
                    return cmd.ExecuteNonQuery().ToString();
                }//using
            }//using
        }//NonQuery()

        protected virtual int GetSongId()
        {
            int current = songId;
            songId++;
            return songId;
        }

        public virtual void AddValues(string[] values)
        {
            this.values.Add(values);
        }
    }
}
