use datatransformation2;

select artists.artist_name, p.counted from
	(select playback.artist_id, count(playback.artist_id) as 'counted' from playback
		group by playback.artist_id) as p
	join artists on artists.artist_id = p.artist_id
    order by p.counted desc;
