﻿using DataTransformation.DbConnectionHelpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransformation
{
    class Program
    {
        static string connectionString = @"Server=localhost;Port=3360;Database=datatransformation3;Uid=root;Pwd=sichiche8;";
        static string dataDirectory = @"C:\Users\uzytkownik\source\repos\DataTransformation\DataTransformation\data";
        static void Main(string[] args)
        {
            //CountLinesAndReadTime(@"..\..\data\unique_tracks.txt");
            //CountLinesAndReadTime(@"..\..\data\triplets_sample_20p.txt", 0, true, 5);
            try
            {
                Console.WriteLine("\nLoading unique_tracks.txt\n");
                UniqueTracksLoader dbObject = new UniqueTracksLoader(connectionString);
                //LoadAndTransformUniqueTracks(@"..\..\data\unique_tracks.txt", "<SEP>", dbObject, 100000, 0, false);
                //LoadAndTransformUniqueTracks(@"..\..\data\unique_tracks.txt", "<SEP>", dbObject);
                LoadAndTransformUniqueTracks(dataDirectory + @"\unique_tracks.txt", "<SEP>", dbObject);

                Console.WriteLine("\nLoading triplets_sample_20p.txt\n");
                TripletsLoader tripltes = new TripletsLoader(connectionString, dbObject.GetSongKeys(), dbObject.GetArtistsKeys(), dbObject.GetSongsArtists());
                //LoadAndTransformUniqueTracks(@"..\..\data\triplets_sample_20p.txt", "<SEP>", tripltes, 0, 5, true);
                //LoadAndTransformUniqueTracks(@"..\..\data\triplets_sample_20p.txt", "<SEP>", tripltes, 200000, 0, false);
                LoadAndTransformUniqueTracks(dataDirectory + @"\triplets_sample_20p.txt", "<SEP>", tripltes, 500000, 4000000, false);

                PressKeyToContinue();
            } catch(Exception e)
            {
                Console.WriteLine("\n ========================= ");
                Console.WriteLine(e.ToString());
            }
        }

        private static void LoadAndTransformUniqueTracks(string filename, string separator, UniqueTracksLoader db, int infoStep = 0, int lines = 0, bool writeLine = false)
        {
            Stopwatch sw = new Stopwatch();
            int counter = 0;
            string line;

            sw.Start();
            StreamReader file = new StreamReader(filename);
            while ((line = file.ReadLine()) != null)
            {
                //string[] record = line.Split(separator, StringSplitOptions.None);
                var rx = new System.Text.RegularExpressions.Regex(separator);
                var record = rx.Split(line);
                db.AddValues(record);

                counter++;
                if (writeLine)
                    Console.WriteLine(line);
                if (infoStep != 0 && counter % infoStep == 0)
                {
                    Console.WriteLine(db.InsertData());
                    Console.WriteLine("Readed and added {0} lines", counter);
                }//if
                if (lines != 0 && counter == lines)
                    break;
            }//while

            file.Close();
            Console.WriteLine(db.InsertData());
            sw.Stop();
        }

        private static void PressKeyToContinue()
        {
            Console.WriteLine("Press any key to continue");
            Console.ReadLine();
        }

        private static void CountLinesAndReadTime(string filename, int infoStep = 0, bool writeLine = false, int breakAfter = 0, string separator = "<SEP>")
        {
            Stopwatch sw = new Stopwatch();
            int counter = 0;
            int longestStringSize = 0;
            string line;

            sw.Start();
            StreamReader file = new StreamReader(filename);
            while ((line = file.ReadLine()) != null)
            {
                counter++;
                if (writeLine)
                    Console.WriteLine(line);
                if (infoStep != 0 && counter % infoStep == 0)
                    Console.WriteLine("Readed {0} lines", counter);
                if (breakAfter != 0 && counter == breakAfter)
                    break;

                int currentRecordLongestStringSize = getLongestStringSize(line);
                if (currentRecordLongestStringSize > longestStringSize)
                    longestStringSize = currentRecordLongestStringSize;
            }//while

            file.Close();
            sw.Stop();
            Console.WriteLine("File: {0}", filename);
            Console.WriteLine("There were {0} lines.", counter);
            Console.WriteLine("Reading time: {0}", sw.Elapsed);
            Console.WriteLine("Longest string size: {0}", longestStringSize);
        }

        private static int getLongestStringSize(string line, string separator = "<SEP>")
        {
            var rx = new System.Text.RegularExpressions.Regex(separator);
            var record = rx.Split(line);
            int maxSize = 0;
            foreach(string s in record)
            {
                if (maxSize < s.Length)
                    maxSize = s.Length;
            }//foreach

            return maxSize;
        }
    }
}
