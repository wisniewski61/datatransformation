use datatransformation;

create table UniqueTracks(
	id int,
	id_utworu varchar(512),
    id_wykonania varchar(512),
    artist varchar(512),
    title varchar(512)
);

create table triplets_sample_20p(
	id int,
	user_id varchar(512),
    song_id varchar(512),
    listen_time varchar(512)
);

create table Songs(
	song_id int,
    old_song_id varchar(512),
    artist_id int,
	title varchar(512)
);

create table Artists(
	artist_id int,
	artist_name varchar(512)
);