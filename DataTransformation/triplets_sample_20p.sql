use datatransformation;

create table dates(
	id int,
	year_ year,
    month_ int,
    day_ int,
    hour_ int,
    unix_timestamp bigint
);

create table users(
	id int,
    old_id varchar(64)
);

create table playback(
	id int,
    user_id int,
    song_id int,
    artist_id int,
    date_id int
);

