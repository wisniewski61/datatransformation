use datatransformation2;
    
select p.user_id, count(p.user_id) from 
	(select playback.user_id, count(playback.song_id) from playback
		group by playback.song_id, playback.user_id) as p
    group by p.user_id
    order by count(p.user_id) desc;
    
select user_id, count(distinct song_id) as 'counted' from playback
group by user_id
order by counted desc