use datatransformation2;

select dates.month_ as 'month', count(playback.id) as 'sum' from playback
	join dates on dates.id = playback.date_id
    group by dates.month_;